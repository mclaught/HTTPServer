/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TestHttp.h
 * Author: user
 *
 * Created on 24 ноября 2020 г., 14:03
 */

#ifndef TESTHTTP_H
#define TESTHTTP_H

#include "MySimpleHttp.h"

class TestHttp : public MySimpleHttp {
public:
    TestHttp();
    TestHttp(const TestHttp& orig);
    virtual ~TestHttp();
    
    virtual void onRequest(MyHttpRequest &request, MyHttpResponse &response);
private:

};

#endif /* TESTHTTP_H */

