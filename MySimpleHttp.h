/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MySimpleHttp.h
 * Author: user
 *
 * Created on 24 ноября 2020 г., 11:38
 */

#ifndef MYSIMPLEHTTP_H
#define MYSIMPLEHTTP_H

#include <set>
#include <map>
#include <vector>

#include "MyThread.h"

using namespace std;

class MyHttpRequest{
public:
    MyHttpRequest(){}
    MyHttpRequest(const MyHttpRequest& orig){}
    virtual ~MyHttpRequest(){}
    
    string method;
    vector<string> path;
    map<string, string> headers;
    map<string, string> get;
    map<string, string> post;
    
    int content_length = 0;
    
    string body;
};

class MyHttpResponse{
public:
    MyHttpResponse(){}
    MyHttpResponse(const MyHttpResponse& orig){}
    virtual ~MyHttpResponse(){}
    
    int code;
    string reason;
    map<string, string> headers;
    string body;
};

class MySimpleHttp : public MyThread {
public:
    MySimpleHttp(uint16_t port);
    MySimpleHttp(const MySimpleHttp& orig);
    virtual ~MySimpleHttp();
    
    virtual void onRequest(MyHttpRequest &request, MyHttpResponse &response){}
private:
    int sock;
    int cli_sock;
    
    void exec();
    void proceesClient(int cli_sock);
    vector<string> split(string str, string delim);
    
    void processRequest(MyHttpRequest &request);
};

#endif /* MYSIMPLEHTTP_H */

