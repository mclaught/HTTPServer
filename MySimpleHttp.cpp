/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MySimpleHttp.cpp
 * Author: user
 * 
 * Created on 24 ноября 2020 г., 11:38
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <exception>
#include <cstring>
#include <netinet/in.h>
#include <iostream>
#include <algorithm>
#include <unistd.h>

#include "MySimpleHttp.h"

MySimpleHttp::MySimpleHttp(uint16_t port) : MyThread() {
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1)
        throw runtime_error(strerror(errno));
    
    int arg = 1;
    if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &arg, sizeof(int)) == -1){
        cerr << "Error setsockopt: " << strerror(errno) << endl;
        exit(1);
    }
    if(setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &arg, sizeof(int)) == -1){
        cerr << "Error setsockopt: " << strerror(errno) << endl;
        exit(1);
    }
    
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    if(bind(sock, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) == -1)
        throw runtime_error(strerror(errno));
}

MySimpleHttp::MySimpleHttp(const MySimpleHttp& orig) : MyThread() {
}

MySimpleHttp::~MySimpleHttp() {
}

void MySimpleHttp::exec(){
    if(listen(sock, 10) == -1)
        throw runtime_error(strerror(errno));
    
    while(!terminated){
        fd_set rd_set;
        FD_ZERO(&rd_set);
        FD_SET(sock, &rd_set);
        int max_sock = sock;
        
        timeval tv = {1,0};
        
        int sel_cnt = select(max_sock+1, &rd_set, nullptr, nullptr, &tv);
        if(sel_cnt == -1)
            cerr << strerror(errno);
        else if(sel_cnt > 0){
            if(FD_ISSET(sock, &rd_set)){
                sockaddr_in cli_addr;
                memset(&cli_addr, 0, sizeof(sockaddr_in));
                socklen_t len = sizeof(sockaddr_in);
                cli_sock = accept(sock, reinterpret_cast<sockaddr*>(&cli_addr), &len);
                if(cli_sock == -1)
                    cerr << strerror(errno);
                else
                    proceesClient(cli_sock);
            }
        }
    }
}

void MySimpleHttp::proceesClient(int cli_sock){
    string str;
    MyHttpRequest request;
    int state = 0;
    while(!terminated){
//        fd_set rd_set;
//        FD_ZERO(&rd_set);
//        FD_SET(cli_sock, &rd_set);
//        int max_sock = cli_sock;
//        
//        timeval tv = {0,100000};
//        
//        int sel_cnt = select(max_sock+1, &rd_set, nullptr, nullptr, &tv);
//        if(sel_cnt == -1)
//            cerr << strerror(errno);
//        else {
//            if(FD_ISSET(cli_sock, &rd_set)){
                char c;
                auto rd = recv(cli_sock, &c, 1, 0);
                if(rd == -1)
                    cerr << strerror(errno);
                else if(rd == 0)
                    return;
                else{
                    switch(state){
                        case 0:{
                            str += c;
                            if(c == '\n'){
                                vector<string> hdr = split(str, " ");
                                request.method = hdr.size()>0 ? hdr[0] : "";
                                if(hdr.size()>1){
                                    vector<string> path_parts = split(hdr[1], "?");
                                    if(path_parts.size()>0)
                                        request.path = split(path_parts[0], "/");
                                    if(path_parts.size()>1){
                                        vector<string> get_parts = split(path_parts[1], "&");
                                        for(auto s : get_parts){
                                            vector<string> prm_parts = split(s,"=");
                                            if(prm_parts.size()>1){
                                                request.get[prm_parts[0]] = prm_parts[1];
                                            }
                                        }
                                    }
                                }
                                
                                str = "";
                                state = 1;
                            }
                            break;
                        }
                        
                        case 1:{
                            str += c;
                            if(c == '\n'){
                                vector<string> parts = split(str, ": ");
                                if(parts.size()>1){
                                    request.headers[parts[0]] = parts[1];
                                    if(parts[0] == "Content-Length"){
                                        request.content_length = stoi(parts[1]);
                                    }
                                }
                                if(str == "\r\n"){
                                    if(request.content_length){
                                        state = 2;
                                    }else{
                                        processRequest(request);
                                        return;
                                    }
                                }
                                str = "";
                            }
                            break;
                        }
                        
                        case 2:{
                            request.body += c;
                            if(request.body.size() >= request.content_length){
                                processRequest(request);
                                return;
                            }
                            break;
                        }
                    }
                }
//            }
//        }
    }
}

void MySimpleHttp::processRequest(MyHttpRequest &request){
    MyHttpResponse response;
    response.code = 200;
    response.reason = "OK";
    
    response.headers["Content-type"] = "text/html";
    response.headers["Cache-Control"] = "no-cache";
    response.headers["Connection"] = "close";
    
    onRequest(request, response);
    
    response.headers["Content-Length"] = to_string(response.body.size());
    
    string answer = "HTTP/1.0 "+to_string(response.code)+" "+response.reason+"\r\n";
    for(auto p : response.headers){
        answer += p.first+": "+p.second+"\r\n";
    }
    answer += "\r\n";
    answer += response.body;
    
    if(send(cli_sock, answer.c_str(), answer.size(), 0) == -1)
        cerr << strerror(errno) << endl;
    
    close(cli_sock);
}

vector<string> MySimpleHttp::split(string str, string delim){
    vector<string> res;
    int delim_sz = delim.length();
    while(str.length()){
        int n = str.find(delim);
        if(n != string::npos){
            res.push_back(str.substr(0, n));
            str.erase(0, n+delim_sz);
        }else{
            res.push_back(str);
            break;
        }
    }
    return res;
}
