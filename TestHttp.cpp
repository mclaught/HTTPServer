/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TestHttp.cpp
 * Author: user
 * 
 * Created on 24 ноября 2020 г., 14:03
 */

#include "TestHttp.h"

TestHttp::TestHttp() : MySimpleHttp(8888) {
}

TestHttp::TestHttp(const TestHttp& orig) : MySimpleHttp(8888) {
}

TestHttp::~TestHttp() {
}

void TestHttp::onRequest(MyHttpRequest &request, MyHttpResponse &response){
    response.body = "Test server answer";
}